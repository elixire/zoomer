import express from "express";
import * as env from "./src/env";

import bodyParser from "body-parser";
import morgan from "morgan";

export const app = express();

app.use("/api", bodyParser.json());
if (env.env == "development") {
  app.use(morgan("dev"));
} else if (env.env == "production") {
  app.use(morgan("tiny"));
} else if (env.env == "test") {
  app.use(morgan("short"));
}

import "./src/routers";
import "./src/endpoints";

app.listen(
  env.port,
  env.env == "development" ? "0.0.0.0" : "127.0.0.1",
  function() {
    console.log(
      "Listening on port",
      env.port,
      "on",
      env.env == "development" ? "all interfaces" : "loopback interface"
    );
  }
);
