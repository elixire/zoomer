module.exports = {
  type: "postgres",
  // just for clarity
  name: "zoomer",
  entities: ["src/entities/**/*.ts"],
  migrations: ["migrations/*.ts"],
  logging: process.env.NODE_ENV != "production",
  // DANGER! ONLY SYNC WHEN DEVELOPMENT ENV IS EXPLICITLY DECLARED
  synchronize: process.env.NODE_ENV == "development",
  // ...migrations, however, should be safe enough, but may annoy devs
  migrationsRun: true, // process.env.NODE_ENV && process.env.NODE_ENV != "development",
  // we explicitly declare cacheable queries as that might be too-controversial
  cache: false,

  database: "zoomer_" + (process.env.NODE_ENV || "development"),
  username: "zoomer",
  password: "zoomer"
};
