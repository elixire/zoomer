import "./api/hello";
import "./api/login";
import "./api/apikey";
import "./api/register";
import "./api/profile";
import "./api/stats";
import "./api/domains";
import "./api/upload";
import "./api/shorten";
import "./api/delete";
import "./api/dump/status";
import "./api/admin/domains";

import "./api/abnormalities/image";
import "./api/abnormalities/shorten";

import "./api/init";
