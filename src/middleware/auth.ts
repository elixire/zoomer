import {validateToken} from "../utils/token";
import {Users} from "../entities/User";
import {Request, Response, NextFunction} from "../Request";
import {toBigIntBE} from "bigint-buffer";

type AuthenticationOptions = {
  // Is authentication required on this route?
  optional?: boolean;
  // Should the user must be an administrator to pass?
  admin?: boolean;
};

// TODO: this should probably be refactored
export function authenticate(
  options: AuthenticationOptions = {optional: false, admin: false}
) {
  return async function authenticateMiddleware(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const token =
      req.query.token ||
      req.query.key ||
      req.headers.authorization ||
      req.cookies.token;

    if (!token && options.optional) {
      req.user = null;
      return next();
    }

    const untimed = token.startsWith("u");

    const userId = token.substring(untimed ? 1 : 0, token.indexOf("."));

    const user = await Users.findOne({
      id: userId
    });

    if (user) {
      // they're bad
      if (!user.active) {
        req.user = null;
      } else {
        if (
          validateToken(token, user.password, (untimed ? "u" : "") + user.id)
        ) {
          if (!untimed) {
            const date = Buffer.from(
              token.substring(token.indexOf("."), token.lastIndexOf(".") - 1),
              "base64"
            );
            // is the token more than 1 month old?
            if (toBigIntBE(date) < Date.now() + 2629800000) {
              req.user = null;
            } else {
              req.user = user;
            }
          } else {
            req.user = user;
          }
        } else {
          if (!options.optional) {
            res.status(403).json({
              error: true,
              message: "Invalid token"
            });
          }
        }
      }
    }

    if (!req.user) {
      if (options.optional) {
        req.user = null;
        return next();
      } else {
        // Shouldn't happen?
        return res.status(403).json({
          error: true,
          message: "Invalid token"
        });
      }
    }

    if (options.admin) {
      if (req.user.admin) {
        return next();
      } else if (options.optional) {
        req.user = null;
        return next();
      } else {
        return res.status(403).json({
          error: true,
          message: "Admin-only route"
        });
      }
    }

    // No special handling...
    return next();
  };
}
