import {createConnection, Connection} from "typeorm";
import {Client as MinioClient} from "minio";
import * as logger from "./utils/logger";
import * as env from "./env";

export let connection = process.argv.some(x => x.includes("typeorm"))
  ? new Promise<Connection>(() => {})
  : createConnection("zoomer");
export const minio = new MinioClient({
  endPoint: env.objectStorage.endpoint,
  port: env.objectStorage.port,
  useSSL: env.objectStorage.useSSL || false,
  accessKey: env.objectStorage.auth.access,
  secretKey: env.objectStorage.auth.secret
});
minio
  .bucketExists(env.fileBucket)
  .then(
    (exists: boolean) => (
      exists || minio.makeBucket(env.fileBucket, env.objectStorage.region),
      undefined
    )
  )
  .then(() => {
    logger.info("Minio ready");
  });
export default connection;
