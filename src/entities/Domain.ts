import {Entity, Repository, Column} from "typeorm";
import connection from "../database";

@Entity()
export default class Domain {
  // incremental blegh
  @Column({
    primary: true,
    unique: true,
    type: "varchar"
  })
  id: string | number;

  @Column({
    unique: true,
    length: 20
  })
  domain: string;

  @Column()
  ownerId: string;

  // @ManyToOne(type => User, {onDelete: "CASCADE"})
  // @JoinColumn()
  // owner: Promise<User>;

  @Column({default: false})
  adminOnly: boolean;

  @Column({default: false})
  official: boolean;

  @Column({default: false})
  wildcard: boolean;

  @Column({default: 3})
  permissions: number;
}

// sorryyyy
export let Domains: Repository<Domain>;
connection.then(conn => {
  Domains = conn.getRepository(Domain);
});
