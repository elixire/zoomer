import {Repository, Entity, JoinColumn, ManyToOne, Column} from "typeorm";
import User from "./User";
import Domain from "./Domain";
import connection from "../database";

@Entity()
export default class Shorten {
  @Column({
    primary: true,
    unique: true,
    type: "varchar"
  })
  id: string | number;

  @Column({
    default: 0
  })
  domainId: Domain["id"] | null;

  @ManyToOne(type => Domain, {
    onDelete: "SET NULL"
  })
  @JoinColumn()
  domain: Promise<Domain | null>;

  @Column({
    type: "string"
  })
  ownerId: User["id"] | null;

  // if you delete your account,
  // your shortens go with it
  @ManyToOne(type => User, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  owner: Promise<User | null>;

  @Column()
  location: string;

  @Column()
  shortcode: string;

  @Column({type: "varchar"})
  subdomain: string | null;
}

// sorryyyy
export let Shortens: Repository<Shorten>;
connection.then(conn => {
  Shortens = conn.getRepository(Shorten);
});
