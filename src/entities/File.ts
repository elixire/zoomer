import {Repository, Entity, JoinColumn, ManyToOne, Column} from "typeorm";
import User from "./User";
import Domain from "./Domain";
import connection from "../database";

@Entity()
export default class File {
  @Column({
    primary: true,
    unique: true,
    type: "varchar"
  })
  id: string;

  @Column({
    nullable: true,
    type: "varchar"
  })
  domainId: Domain["id"] | null;

  @ManyToOne(type => Domain, {onDelete: "SET NULL"})
  @JoinColumn()
  domain: Promise<Domain>;

  @Column({
    type: "varchar"
  })
  ownerId: User["id"] | null;

  // if you delete your account,
  // your files go with it
  @ManyToOne(type => User, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  owner: Promise<User | null>;

  @Column({comment: "size in bytes"})
  size: number;

  @Column({comment: "the file's shortcode"})
  shortcode: string;

  @Column({comment: "canonical file extension"})
  extension: string;

  @Column({
    comment: "Subdomain to limit file to",
    type: "varchar",
    default: null,
    nullable: true
  })
  subdomain: string | null;

  @Column({
    type: "varchar",
    default: "application/octet-stream"
  })
  mime: string;

  @Column({
    comment: "Should the file be served? (false on deletion)",
    default: true
  })
  available: boolean;
}

// sorryyyy
export let Files: Repository<File>;
connection.then(conn => {
  Files = conn.getRepository(File);
});
