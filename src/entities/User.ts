import {JoinColumn, ManyToOne, Entity, Column, Repository} from "typeorm";
import connection from "../database";
import Domain from "./Domain";

@Entity()
export default class User {
  @Column({
    primary: true,
    unique: true
  })
  id: string;

  @Column({
    unique: true,
    length: 20
  })
  username: string;

  @Column({
    unique: true
  })
  email: string;

  @Column()
  password: string;

  @Column({default: false})
  admin: boolean;

  @Column({default: false})
  active: boolean;

  @Column({default: 104857600})
  fileLimit: number;

  @Column({default: 100})
  shortenLimit: number;

  @Column({default: "0"})
  domainId?: Domain["id"] | null;

  @ManyToOne(type => Domain, {
    onDelete: "SET NULL"
  })
  @JoinColumn()
  domain: Promise<Domain | null>;

  @Column({default: "0"})
  shortenDomainId?: Domain["id"] | null;

  @ManyToOne(type => Domain, {
    onDelete: "SET NULL"
  })
  @JoinColumn()
  shortenDomain: Promise<Domain | null>;

  @Column({default: ""})
  subdomain: string;

  @Column({default: ""})
  shortenSubdomain: string;

  @Column({default: null, nullable: true, type: "boolean"})
  consented?: boolean | null;

  @Column({default: false})
  paranoid: boolean;
}

// sorryyyy
export let Users: Repository<User>;
connection.then(conn => {
  Users = conn.getRepository(User);
});
