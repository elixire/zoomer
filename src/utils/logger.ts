export const info = console.log.bind(console, "[info]");
export const error = console.error.bind(console, "[error]");
export const warn = console.warn.bind(console, "[warn]");
