import * as Yup from "yup";
import {NextFunction, Request, Response} from "../Request";

type LegacySchemaError = {
  // unfortunately, typescript is fucking stupid and doesn't like the old way
  // (which was a silly way of doing it anyways, but oh well)
  [field: string]: string[] | boolean | string;
  error: boolean;
  message: string;
};
type Details = {
  [field: string]: string[];
};
type SchemaError = {
  error: boolean;
  message: string;
  detail?: Details;
};

export function validate(schema: Yup.Schema<any>) {
  return async function validateMiddleware(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      await schema.validate(req.body, {
        // love that backwards compat
        abortEarly: false,
        // may as well
        stripUnknown: true
      });
    } catch (err) {
      const detail: Details = {};

      for (const schemaError of err.inner) {
        detail[schemaError.path] = schemaError.errors;
      }

      const error: LegacySchemaError | SchemaError = {
        error: true,
        message: "Invalid request body"
      };

      if (
        req.originalUrl.startsWith("/api/v2") ||
        !req.originalUrl.startsWith("/api/v")
      ) {
        Object.assign(error, detail);
      } else {
        error.detail = detail;
      }

      res.status(422).json(error);
      return;
    }

    next();
  };
}

export const yup = Yup;

export const schemas = {
  username: yup
    .string()
    .min(3)
    .max(20)
    .matches(/^[a-zA-Z0-9]{1}[a-zA-Z0-9_]{2,19}$/),
  password: yup
    .string()
    .min(8)
    // why 72? who fucking knows. I don't see a need, but oh well
    .max(72),
  email: yup
    .string()
    .matches(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/),
  discordTag: yup
    .string()
    .min(2 + 4)
    .max(32 + 4)
    .matches(/^[^\#]{2,70}\#\d{4}$/),
  // TODO: write a decent schema for domains
  domain: yup.string(),
  subdomain: yup
    .string()
    .min(0)
    .max(63)
    .matches(/^([a-zA-Z0-9_][a-zA-Z0-9_-]{0,61}[a-zA-Z0-9_]|[a-zA-Z0-9_]|)$/)
};
