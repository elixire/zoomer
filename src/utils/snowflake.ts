import Flake from "flake-idgen";
import * as env from "../env";
// Needed for efficient buffer->bigint conversion
import {toBigIntBE} from "bigint-buffer";

const flake = new Flake({
  epoch: env.epoch
});

export function generateSnowflake() {
  return new Promise<string>(function(resolve, reject) {
    flake.next(function(err: Error, id: Buffer) {
      if (err) reject(err);
      else resolve(toBigIntBE(id).toString());
    });
  });
}
