import * as env from "../env";
import * as crypto from "crypto";

function generateMagic(userId: string, secret: string) {
  function signature(key: string | Buffer, data: string | Buffer) {
    const hmacAlgo = crypto.createHmac("sha1", key);
    hmacAlgo.update(data);
    return hmacAlgo.digest();
  }

  const buffer = Buffer.from(
    signature(signature(env.serverSalt, secret), userId)
  );
  return buffer;
}

export function generateToken(userId: string, secret: string) {
  const time = Buffer.from(
    (new Date().getTime() - env.epoch).toString(16),
    "hex"
  ).toString("base64");
  return (
    userId +
    "." +
    time +
    "." +
    generateMagic(userId + "." + time, secret).toString("base64")
  );
}

export function validateToken(token: string, secret: string, userId: string) {
  const lastDotIndex = token.lastIndexOf(".");
  const magic = token.substring(lastDotIndex + 1);
  try {
    var oldBuffer = Buffer.from(magic, "base64");
  } catch (err) {
    // Basically just means it's not b64'd
    return false;
  }
  const newBuffer = generateMagic(token.substring(0, lastDotIndex), secret);
  if (oldBuffer.length != newBuffer.length) return false;

  return crypto.timingSafeEqual(oldBuffer, newBuffer);
}
