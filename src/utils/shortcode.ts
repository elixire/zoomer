import File from "../entities/File";
import Shorten from "../entities/Shorten";
import {Repository} from "typeorm";

export const alphabet = "abcdefghijklmnopqrstuvwxyz0123456789".split("");

export async function generateShortcode(
  iterationCount: number,
  characterCount: number,
  Repository: Repository<File> | Repository<Shorten>
) {
  let code = null;
  for (let i = 0; i < iterationCount; i++) {
    code = "";
    for (let c = 0; c < characterCount; c++) {
      code += alphabet[Math.floor(Math.random() * alphabet.length)];
      console.log("appended", code);
    }
    console.log("code round...", code);
    const exists = await Repository.createQueryBuilder("item")
      .select("COUNT(1)", "exists")
      .where("item.shortcode = :code", {code})
      .getRawOne()
      .then((data: {exists: string}) => !!Number(data.exists));
    if (!exists) {
      break;
    } else {
      code = null;
    }
  }
  if (!code) {
    throw new ShortcodeExhaustion("Couldn't find an unused shortcode in time");
  }
  return code;
}

export class ShortcodeExhaustion extends Error {
  constructor(msg: string) {
    super(msg);
    this.name = this.constructor.name;
  }
}
