import {v2} from "../routers";
import {generateToken} from "../utils/token";
import {Users} from "../entities/User";
import {yup, validate, schemas} from "../utils/schema";
import {LoginBody} from "./login";
import bcrypt from "bcrypt";
import {Request, Response} from "../Request";

v2.post(
  "/apikey",
  validate(
    yup.object().shape({
      user: schemas.username.clone().required(),
      password: schemas.password.clone().required()
    })
  ),
  async function(req: Request, res: Response) {
    const body = req.body as LoginBody;

    const user = await Users.findOne({
      username: body.user
    });

    if (!user) {
      return res.status(403).json({
        error: true,
        // Never know when you might 2fa!
        message: "Invalid credentials"
      });
    }

    const success = await bcrypt.compare(body.password, user.password);
    if (!success) {
      return res.status(403).json({
        error: true,
        message: "Invalid credentials"
      });
    } else if (!user.active) {
      return res.status(403).json({
        error: true,
        message:
          "Account is not activated. Please be patient and await your activation email!"
      });
    }

    const token = generateToken("u" + user.id, user.password);

    return res.json({
      // TODO: inconsistent
      api_key: token
    });
  }
);
