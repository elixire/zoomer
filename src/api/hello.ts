import {v2, v3} from "../routers";
import * as env from "../env";
import * as pkg from "../../package.json";
import * as express from "express";

function generateHello() {
  return {
    name: env.instance.name,
    version: pkg.version,
    api: null,
    invite: env.instance.discordInvite,
    // camelCase whenplz
    support_email: env.instance.supportEmail,

    ban_period: env.ratelimits.standard.time,
    // this accomplishes nothing
    ip_ban_period: 0,
    rl_threshold: env.ratelimits.standard.perMinute,

    accepted_mimes: env.mimes
  };
}

// Why does this route exist?

v2.get("/hello", function(req: express.Request, res: express.Response) {
  return res.json(
    Object.assign(generateHello(), {
      // I doubt anything depends on this, but just in case...
      api: "2019.2.4.9"
    })
  );
});

v3.get("/hello", function(req: express.Request, res: express.Response) {
  return res.json(
    Object.assign(generateHello(), {
      // report an actual version
      api: pkg.version
    })
  );
});
