import {imageRouter} from "../../routers";
import * as logger from "../../utils/logger";
import {Request, Response} from "../../Request";
import {Files} from "../../entities/File";
import {Domains} from "../../entities/Domain";
import {minio} from "../../database";
import {In} from "typeorm";
import * as env from "../../env";

type RequestParams = {
  shortcode: string;
  extension: string;
};

imageRouter.get("/i/:shortcode.:extension", async function(
  req: Request,
  res: Response
) {
  const params = req.params as RequestParams;

  const lastColon = req.headers.host.lastIndexOf(":");
  const hostname =
    lastColon == -1
      ? req.headers.host
      : req.headers.host.substring(0, lastColon);

  const candidateDomain = hostname.substring(hostname.indexOf("."));

  const domain = await Domains.findOne({
    where: {
      domain: In([hostname, candidateDomain])
    },
    cache: true
  });

  if (!domain) {
    return res.status(404).json({
      error: true,
      message: "The requested file could not be found on this domain"
    });
  }

  const file = await Files.createQueryBuilder("file")
    .where(
      "file.shortcode = :shortcode AND file.extension = :extension AND file.domainId = :domainId AND (file.subdomain IS NULL OR file.subdomain = :subdomain) AND file.available = true",
      {
        shortcode: params.shortcode,
        extension: "." + params.extension,
        domainId: domain.id,
        subdomain: hostname.slice(0, -domain.domain.length - 1)
      }
    )
    .cache(true)
    .getOne();

  if (!file) {
    return res.status(404).json({
      error: true,
      message: "The requested file could not be found"
    });
  }

  try {
    const stream = await minio.getObject(env.fileBucket, file.shortcode);

    res.set("Content-Type", file.mime);
    return stream.pipe(res);
  } catch (err) {
    logger.warn(
      "Perhaps something has gone wrong on storage backend? File exists in DB but errored while retrieving...",
      err
    );
    return res.status(503).json({
      error: true,
      message:
        "An error occured while fetching the file from the storage backend"
    });
  }
});
