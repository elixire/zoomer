import {imageRouter} from "../../routers";
import {Request, Response} from "../../Request";
import {Shortens} from "../../entities/Shorten";
import {Domains} from "../../entities/Domain";
import {In} from "typeorm";

type RequestParams = {
  shortcode: string;
};

imageRouter.get("/s/:shortcode", async function(req: Request, res: Response) {
  const params = req.params as RequestParams;

  const lastColon = req.headers.host.lastIndexOf(":");
  const hostname =
    lastColon == -1
      ? req.headers.host
      : req.headers.host.substring(0, lastColon);

  const candidateDomain = hostname.substring(hostname.indexOf("."));

  const domain = await Domains.findOne({
    where: {
      domain: In([hostname, candidateDomain])
    },
    cache: true
  });

  if (!domain) {
    return res.status(404).json({
      error: true,
      message: "The requested shorten could not be found on this domain"
    });
  }

  const shorten = await Shortens.createQueryBuilder("shorten")
    .where(
      "shorten.shortcode = :shortcode AND shorten.domainId = :domainId AND (shorten.subdomain IS NULL OR shorten.subdomain = :subdomain)",
      {
        shortcode: params.shortcode,
        domainId: domain.id,
        subdomain: hostname.slice(0, -domain.domain.length - 1)
      }
    )
    .cache(true)
    .getOne();

  if (!shorten) {
    return res.status(404).json({
      error: true,
      message: "The requested shorten could not be found"
    });
  }

  return res.redirect(301, shorten.location);
});
