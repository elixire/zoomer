import {v2} from "../routers";
import {Request, Response} from "../Request";
import {authenticate} from "../middleware/auth";
import {yup, validate} from "../utils/schema";
import {Files} from "../entities/File";

type LegacyDelete = {
  // misnomer, should be shortcode
  filename: string;
};

type Delete = {
  shortcode: string;
};

v2.delete(
  "/delete",
  validate(
    yup.object().shape({
      filename: yup.string().required()
    })
  ),
  authenticate(),
  async function(req: Request, res: Response) {
    const body = req.body as LegacyDelete;
    return await deleteFile(body.filename, req, res);
  }
);

v2.get("/delete/:shortcode", authenticate(), async function(
  req: Request,
  res: Response
) {
  const params = req.params as Delete;
  return await deleteFile(params.shortcode, req, res);
});

v2.delete("/delete/:shortcode", authenticate(), async function(
  req: Request,
  res: Response
) {
  const params = req.params as Delete;
  return await deleteFile(params.shortcode, req, res);
});

async function deleteFile(shortcode: string, req: Request, res: Response) {
  const exists = await Files.createQueryBuilder("file")
    .select("COUNT(1)", "exists")
    .where("file.shortcode = :shortcode AND file.ownerId = :ownerId", {
      shortcode,
      ownerId: req.user.id
    })
    .getRawOne()
    .then((data: {exists: string}) => !!Number(data.exists));

  // potential enumeration vector
  if (!exists) {
    return res.status(404).json({
      message: "Requested file does not exist or you do not own it"
    });
  }

  await Files.update({shortcode, ownerId: req.user.id}, {available: false});

  return res.json({
    success: true
  });
}
