import {Domains} from "../entities/Domain";
import {v3} from "../routers";
import {Request, Response} from "../Request";

v3.post("/_init", async function(req: Request, res: Response) {
  if (req.headers.host != "invalid.tld") {
    return res.status(404).json({
      error: true,
      message: "Not Found"
    });
  }

  await Domains.save({
    id: "0",
    domain: "100.115.92.198",
    ownerId: "0",
    official: true,
    wildcard: true
  });

  return res.json({success: true});
});
