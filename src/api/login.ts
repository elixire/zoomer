import {v2} from "../routers";
import {Users} from "../entities/User";
import bcrypt from "bcrypt";
import {generateToken} from "../utils/token";
import {yup, schemas, validate} from "../utils/schema";
import {Request, Response} from "../Request";

export type LoginBody = {
  user: string;
  password: string;
};

v2.post(
  "/login",
  validate(
    yup.object().shape({
      user: schemas.username.clone().required(),
      password: schemas.password.clone().required()
    })
  ),
  async function(req: Request, res: Response) {
    const body = req.body as LoginBody;

    const user = await Users.findOne({
      // We can safely assume nobody wants that
      username: body.user.toLowerCase()
    });

    if (!user) {
      return res.status(403).json({
        error: true,
        // Never know when you might 2fa!
        message: "Invalid credentials"
      });
    }

    const success = await bcrypt.compare(body.password, user.password);
    if (!success) {
      return res.status(403).json({
        error: true,
        message: "Invalid credentials"
      });
    } else if (!user.active) {
      return res.status(403).json({
        error: true,
        message:
          "Account is not activated. Please be patient and await your activation email!"
      });
    }

    // TODO: How can we do webauthn and 2fa most elegantly?
    // perhaps: if Authenticators.find({userId: user.id})
    // has a length, we error out providing a list of
    // available authentications methods:
    // {
    //   error: true,
    //   detail: {
    //     mobileAuthenticator?: true
    //     securityKey?: {
    //       challenge: "blah" // b64-encoded challenge
    //       keys: [
    //         "blah1",
    //         "blah2"
    //       ]
    //     }
    //   }
    // }

    const token = generateToken(user.id, user.password);

    return res.json({
      token
    });
  }
);
