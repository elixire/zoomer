import {v2} from "../../routers";
import {authenticate} from "../../middleware/auth";
import {generateSnowflake} from "../../utils/snowflake";
import {validate, yup, schemas} from "../../utils/schema";
import {Domains} from "../../entities/Domain";
import {Request, Response} from "../../Request";

type DomainRequest = {
  domain: string;
  admin_only?: boolean;
  official?: boolean;
  permissions?: number;
  owner_id: string;
};

v2.put(
  "/admin/domains",
  validate(
    yup.object().shape({
      domain: schemas.domain.clone().required(),
      admin_only: yup.boolean(),
      official: yup.boolean(),
      permissions: yup
        .number()
        // no idea what this means, but it's an int
        // TODO: find max() val?
        .min(0)
        .default(3),
      owner_id: yup.string()
    })
  ),
  authenticate({
    admin: true
  }),
  async function(req: Request, res: Response) {
    const body = req.body as DomainRequest;

    const domainId = await generateSnowflake();
    try {
      await Domains.save({
        id: domainId,
        domain: body.domain,
        adminOnly: !!body.admin_only,
        official: !!body.official,
        permissions: body.permissions === undefined ? 3 : body.permissions,
        ownerId: body.owner_id || req.user.id
      });
    } catch (err) {
      console.error(err);

      return res.status(419).json({
        error: true,
        message: "Domain already exists?"
      });
    }

    return res.json({
      success: true,
      result: "what's the field for again?",
      new_id: domainId
    });
  }
);
