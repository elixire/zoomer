import {v2} from "../routers";
import {Users} from "../entities/User";
import {Domains} from "../entities/Domain";
import {Files} from "../entities/File";
import {Shortens} from "../entities/Shorten";
import {authenticate} from "../middleware/auth";
import {validate, yup, schemas} from "../utils/schema";
import {Request, Response} from "../Request";
import bcrypt from "bcrypt";

type ProfilePartial = {
  password?: string;
  domainId?: string;
  subdomain?: string;
  shortenDomainId?: string;
  email?: string;
  consented?: boolean | null;
  paranoid?: boolean;
};

type FileQueryResponse = {
  sum: number;
};

v2.get("/profile", authenticate(), async function(req: Request, res: Response) {
  const [filesUsed, shortenUsed] = await Promise.all([
    Files.createQueryBuilder("file")
      .select("SUM(file.size)", "sum")
      .where("file.ownerId = :id", {id: req.user.id})
      .andWhere("file.id >= :time", {
        time: new Date().getTime() - 604800000
      })
      .getRawOne()
      .then((data: FileQueryResponse) => data.sum || 0),
    Shortens.createQueryBuilder("shorten")
      .where("shorten.ownerId = :id", {id: req.user.id})
      .andWhere("shorten.id >= :time", {
        time: new Date().getTime() - 604800000
      })
      .getCount()
  ]);

  res.json({
    user_id: req.user.id,
    username: req.user.username,
    // ...why is this here?
    active: true,
    email: req.user.email,
    consented: req.user.consented,
    paranoid: req.user.paranoid,

    domain: req.user.domainId,
    subdomain: req.user.subdomain,
    shorten_domain: req.user.shortenDomainId,
    shorten_subdomain: req.user.shortenSubdomain,
    limits: {
      limit: req.user.fileLimit,
      used: filesUsed,
      shortenlimit: req.user.shortenLimit,
      shortenused: shortenUsed
    }
  });
});

type ProfileUpdate = {
  password?: string;
  new_password?: string;
  domain?: string;
  subdomain?: string;
  shorten_domain?: string;
  email?: string;
  consented?: boolean | null;
  paranoid?: boolean;
};

v2.patch(
  "/profile",
  authenticate(),
  validate(
    yup.object().shape({
      password: schemas.password.clone().when("new_password", {
        is: true,
        then: schemas.password.clone().required(),
        otherwise: schemas.password.clone()
      }),
      new_password: schemas.password.clone(),
      domain: schemas.subdomain.clone(),
      subdomain: schemas.subdomain.clone(),
      shorten_domain: schemas.subdomain.clone(),
      email: schemas.email.clone(),
      consented: yup.boolean().nullable(),
      paranoid: yup.boolean()
    })
  ),
  async function(req: Request, res: Response) {
    const body = req.body as ProfileUpdate;
    const delta: ProfilePartial = {};

    const promises = new Set();

    console.log("ProfileUpdate", body, body.domain);

    if (body.new_password) {
      promises.add(
        bcrypt
          .compare(body.password, req.user.password)
          .then(success => {
            if (success) {
              return bcrypt.hash(body.new_password, 11);
            } else {
              return Promise.reject(new Error("Incorrect password"));
            }
          })
          .then(password => {
            delta.password = password;
          })
          .catch(err => {
            res.status(403).json({
              error: true,
              message: "Invalid password"
            });
            return Promise.reject(err);
          })
      );
    }

    if (body.domain !== undefined) {
      console.log("Firing off..");
      const queryBuilder = Domains.createQueryBuilder("domain")
        .select("COUNT(1)", "existence")
        .where("domain.id = :id", {id: body.domain});

      if (!req.user.admin) {
        queryBuilder.andWhere("domain.adminOnly = 0");
      }
      promises.add(
        queryBuilder
          .getRawOne()
          .then(data => Number(data.existence))
          .then(existence => {
            console.log("Got some out", existence);
            if (existence === 1) {
              delta.domainId = body.domain;
              return Promise.resolve();
            } else {
              return Promise.reject(new Error("Domain does not exist"));
            }
          })
          .catch(err => {
            res.status(422).json({
              error: true,
              message: "Domain does not exist with given ID"
            });
            return Promise.reject(err);
          })
      );
    }

    if (body.subdomain !== undefined) {
      delta.subdomain = body.subdomain;
    }

    if (body.shorten_domain !== undefined) {
      const queryBuilder = Domains.createQueryBuilder("domain")
        .select("COUNT(1)", "existence")
        .where("domain.id = :id", {id: body.shorten_domain});

      if (!req.user.admin) {
        queryBuilder.andWhere("domain.adminOnly = 0");
      }
      promises.add(
        queryBuilder
          .getRawOne()
          .then(data => Number(data.existence))
          .then(existence => {
            if (existence === 1) {
              delta.shortenDomainId = body.shorten_domain;
              return Promise.resolve();
            } else {
              return Promise.reject(new Error("Domain does not exist"));
            }
          })
          .catch(err => {
            res.status(422).json({
              error: true,
              message: "Domain does not exist with given ID"
            });
            return Promise.reject(err);
          })
      );
    }

    if (body.email) {
      delta.email = body.email;
    }

    if (body.hasOwnProperty("consented")) {
      delta.consented = body.consented;
    }

    if (body.paranoid !== undefined) {
      delta.paranoid = body.paranoid;
    }

    try {
      await Promise.all(promises.values());
    } catch (err) {
      return;
    }

    console.log("patching user with...", delta);

    await Users.update(
      {
        id: req.user.id
      },
      delta
    );

    res.json({
      updated_fields: Object.keys(body).filter(item => item != "new_password")
    });
  }
);
