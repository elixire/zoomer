import {v2} from "../routers";
import {Files} from "../entities/File";
import {Shortens} from "../entities/Shorten";
import {authenticate} from "../middleware/auth";
import {Request, Response} from "../Request";

v2.get("/stats", authenticate(), async function(req: Request, res: Response) {
  const fileQuery = Files.createQueryBuilder("file")
    .select("SUM(file.size)", "sum")
    .where("file.ownerId = :id", {id: req.user.id});

  const [totalBytes, fileCount, shortenCount] = await Promise.all([
    fileQuery.getRawOne().then(x => x.sum || 0),
    fileQuery.getCount(),
    Shortens.createQueryBuilder("shorten")
      .where("shorten.ownerId = :id", {id: req.user.id})
      .getCount()
  ]);

  return res.json({
    total_bytes: totalBytes,
    total_deleted_files: 0,
    total_files: fileCount,
    total_shortens: shortenCount
  });
});
