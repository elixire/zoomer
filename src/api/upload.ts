import {v2} from "../routers";
import {authenticate} from "../middleware/auth";
import Busboy from "busboy";
import {minio} from "../database";
import {default as File, Files} from "../entities/File";
import * as logger from "../utils/logger";
import {generateSnowflake} from "../utils/snowflake";
import {generateShortcode, ShortcodeExhaustion} from "../utils/shortcode";
import {QuotaError} from "../errors/quota";
import * as env from "../env";
import * as path from "path";
import * as url from "url";
import {Request, Response} from "../Request";
import {Readable} from "stream";

export class FileLimitError extends Error {
  constructor(msg: string) {
    super(msg);
    this.name = this.constructor.name;
  }
}

v2.post("/upload", authenticate(), async function(req: Request, res: Response) {
  try {
    var [file] = await upload(req, res, 1).then(x => x.values());
  } catch (err) {
    if (err._handled) return;
    else throw err;
  }

  res.json({
    url: url
      .format({
        protocol: env.secure ? "https:" : "http:",
        hostname:
          (req.user.subdomain ? req.user.subdomain + "." : "") +
          (await req.user.domain).domain,
        port: env.externalPort,
        pathname: path.join("/i", file.shortcode + file.extension)
      })
      .toString(),
    shortname: file.shortcode,
    delete_url: url
      .format({
        protocol: env.secure ? "https:" : "http:",
        host: req.headers.host,
        port: env.externalPort,
        pathname: path.join("/api/v2/delete", file.shortcode)
      })
      .toString()
  });
});

// share uploader code between api versions
async function upload(req: Request, res: Response, maxFileCount: number) {
  const filePromises = new Map();
  const files = new Map();
  // TODO: Busboy & Readable
  const busboy = new Busboy({
    headers: req.headers
  });

  let quotaAccumulator = 0;

  const busboyDone = new Promise(resolve =>
    busboy.on("finish", () => resolve())
  );

  const limitUsed = await Files.createQueryBuilder("file")
    .select("SUM(file.size)", "sum")
    .where("file.ownerId = :id", {id: req.user.id})
    .andWhere("file.id >= :time", {
      time: new Date().getTime() - 604800000
    })
    .getRawOne()
    .then(x => x.sum || 0);

  const quotaRemaining = req.user.fileLimit - limitUsed;

  const shortcodes: string[] = [];

  busboy.on("file", function(
    fieldName: string,
    file: Readable,
    fileName: string,
    encoding: string,
    mime: string
  ) {
    if (maxFileCount > filePromises.size + 1) {
      file.destroy();
      busboy.destroy();
      filePromises.set(
        fieldName,
        Promise.reject(
          new FileLimitError(
            "A maximum of " +
              filePromises.size +
              " file(s) is allowed per request"
          )
        )
      );
    }
    filePromises.set(
      fieldName,
      generateShortcode(
        10,
        req.user.paranoid ? env.shortcodes.paranoid : env.shortcodes.normal,
        Files
      )
        .then(shortcode => {
          shortcodes.push(shortcode);
          let fileSize = 0;
          return Promise.race([
            // tehcnically inaccurate, but that's alright
            new Promise<{
              fileSize: number;
              shortcode: string;
              fileName: string;
            }>((resolve, reject) => {
              file.on("data", (chunk: Buffer) => {
                quotaAccumulator += chunk.length;
                if (quotaAccumulator >= quotaRemaining) {
                  reject(new QuotaError("File will put user over quota"));
                }
                fileSize += chunk.length;
              });
            }),

            minio
              .putObject(env.fileBucket, shortcode, file, undefined, {
                ownerId: req.user.id,
                "Content-Type": mime
              })
              .then(() => ({fileSize, shortcode, fileName}))
          ]);
        })
        .then(
          async ({
            fileSize,
            shortcode,
            fileName
          }: {
            fileSize: number;
            shortcode: string;
            fileName: string;
          }) => {
            const file = new File();
            file.id = await generateSnowflake();
            file.domainId = req.user.domainId;
            file.ownerId = req.user.id;
            file.owner = req.user;
            file.size = fileSize;
            file.shortcode = shortcode;
            file.subdomain = req.user.subdomain;
            file.extension = path.parse(fileName).ext;
            file.mime = mime;
            await Files.save(file);
            files.set(shortcode, file);
          }
        )
        .catch((err: Error) => {
          file.destroy();
          busboy.destroy();
          // unexpected error
          if (
            !(err instanceof QuotaError) &&
            !(err instanceof ShortcodeExhaustion)
          ) {
            throw err;
          } else {
            return Promise.reject(err);
          }
        })
    );
  });

  req.pipe(busboy);

  await busboyDone;

  try {
    await Promise.all(filePromises.values());
  } catch (err) {
    await Promise.all([
      minio.removeObjects(env.fileBucket, shortcodes),
      Files.remove(Array.from(files.values()))
    ]);

    if (err instanceof QuotaError) {
      res.status(469).json({
        error: true,
        message: "Uploading this file would put you over your quota"
      });
    } else if (err instanceof ShortcodeExhaustion) {
      res.status(403).json({
        error: true,
        message:
          "The file(s) could not be uploaded due to an error while generating a shortcode. Try enabling paranoid mode"
      });
    } else if (err instanceof FileLimitError) {
      res.status(403).json({
        error: true,
        message: err.message
      });
    } else {
      logger.error("Unexpected file upload error:", err);
      res.status(500).json({
        error: true,
        message: "Something went wrong while uploading. Ask your instance admin"
      });
    }

    err._handled = true;

    throw err;
  }

  return files;
}
