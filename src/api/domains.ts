import {v2} from "../routers";
import {Domains} from "../entities/Domain";
import {Request, Response, NextFunction} from "../Request";

v2.get("/domains", async function(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const domains = await Domains.find({});

  res.json({
    domains: Object.fromEntries(
      domains.map(domain => [
        domain.id,
        domain.wildcard ? "*." + domain.domain : domain.domain
      ])
    ),
    // why the fuck are we so inconsistent on this?!
    officialdomains: domains
      .filter(domain => domain.official)
      .map(domain => domain.id)
  });
});
