import {v2} from "../routers";
import {Request, Response} from "../Request";
import {Shortens} from "../entities/Shorten";
import {generateShortcode} from "../utils/shortcode";
import {generateSnowflake} from "../utils/snowflake";
import {authenticate} from "../middleware/auth";
import {yup, validate} from "../utils/schema";
import * as url from "url";
import * as path from "path";
import * as env from "../env";

type RequestBody = {
  url: string;
};

v2.post(
  "/shorten",
  authenticate(),
  validate(
    yup.object().shape({
      url: yup.string().url()
    })
  ),
  async function(req: Request, res: Response) {
    const body = req.body as RequestBody;

    const [shortensUsed, id, shortcode] = await Promise.all([
      Shortens.createQueryBuilder("shorten")
        .where("shorten.ownerId = :id", {id: req.user.id})
        .andWhere("shorten.id >= :time", {
          time: new Date().getTime() - 604800000
        })
        .getCount(),

      await generateSnowflake(),
      await generateShortcode(
        10,
        req.user.paranoid ? env.shortcodes.paranoid : env.shortcodes.normal,
        Shortens
      )
    ]);

    if (shortensUsed >= req.user.shortenLimit) {
      return res.status(469).json({
        message: "Shorten quota reached",
        error: true
      });
    }

    await Shortens.save({
      id,
      domainId: req.user.shortenDomainId,
      ownerId: req.user.id,
      shortcode,
      subdomain: req.user.shortenSubdomain,
      location: body.url
    });

    return res.json({
      url: url
        .format({
          protocol: env.secure ? "https:" : "http:",
          hostname:
            (req.user.shortenSubdomain ? req.user.shortenSubdomain + "." : "") +
            (await req.user.shortenDomain).domain,
          port: env.externalPort,
          pathname: path.join("/s", shortcode)
        })
        .toString()
    });
  }
);
