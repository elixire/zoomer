import {v2} from "../routers";
import {Users} from "../entities/User";
import bcrypt from "bcrypt";
import {validate, yup, schemas} from "../utils/schema";
import {generateSnowflake} from "../utils/snowflake";
import {Request, Response} from "../Request";

type LegacySignupRequest = {
  username: string;
  password: string;
  email: string;
  // oauth wenplz
  discord_user: string;
};

v2.post(
  "/register",
  validate(
    yup.object().shape({
      username: schemas.username.clone().required(),
      password: schemas.password.clone().required(),
      email: schemas.email.clone().required(),
      discord_user: schemas.discordTag.clone().required()
    })
  ),
  async function(req: Request, res: Response) {
    const body = req.body as LegacySignupRequest;

    try {
      await Users.save({
        id: await generateSnowflake(),

        username: body.username.toLowerCase(),
        email: body.email.toLowerCase(),
        password: await bcrypt.hash(body.password, 11),
        // needs to get email confirmed
        active: false,

        shortenDomainId: "0",
        domainId: "0"
      });
    } catch (err) {
      console.error(err);
      // User already exists...
      res.status(409).json({
        error: true,
        message: "This user already exists"
      });
      return;
    }

    res.status(200).json({success: true});
  }
);
