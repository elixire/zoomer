import {v2} from "../../routers";
import {authenticate} from "../../middleware/auth";
import {Request, Response} from "../../Request";

v2.get("/dump/status", authenticate(), async function(
  req: Request,
  res: Response
) {
  res.json({
    state: "not_in_queue"
  });
});
