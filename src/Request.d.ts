import * as express from "express";

type Request = express.Request & ParsedAsJson;
type Response = express.Response;
type NextFunction = express.NextFunction;
