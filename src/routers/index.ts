import {app} from "../../";
import {Request, Response, NextFunction} from "../Request";

// routers
import api3 from "./v3";
import api2 from "./v2";

import imgRouter from "./imageRouter";

export const PRIORITY = [
  {version: 3, router: api3},
  {version: 2, router: api2}
];

app.use("/api", function(req: Request, res: Response, next: NextFunction) {
  res
    .set("Access-Control-Allow-Origin", "*")
    .set(
      "Access-Control-Allow-Methods",
      ["POST", "PATCH", "GET", "DELETE", "PUT"].join(",")
    )
    .set(
      "Access-Control-Allow-Headers",
      ["X-Antichrist-Version", "Authorization", "Content-Type"].join(",")
    );
  if (req.method == "OPTIONS") {
    res.status(204).send("");
  } else {
    next();
  }
});

for (const index in PRIORITY) {
  app.use(
    "/api/v" + PRIORITY[index].version,
    ...PRIORITY.slice(Number(index)).map(({router}) => router)
  );
}

// Use latest on non-versioned routes
app.use("/api", PRIORITY.map(({router}) => router));

// this is so gross
app.use("/", imgRouter);

export const v3 = api3;
// backwards compatability!
export const v2 = api2;
export const imageRouter = imgRouter;
export default v2;
