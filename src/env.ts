// TODO: read from process.env and a gitignore'd file

export const instance = {
  name: "elixi.re",
  discordInvite: "https://discord.gg/xSwFDWx",
  supportEmail: "support@elixi.re"
};

export const ratelimits = {
  standard: {
    time: String(1000 * 60),
    // ...and for good measure
    perMinute: 50
  }
};

// what does this accomplish exactly?
export const mimes = [
  "image/png",
  "image/jpeg",
  "image/gif",
  "image/webp",
  "image/svg+xml",
  "audio/webm",
  "video/webm",
  "video/mp4",
  "application/pgp-keys",
  "text/x-tex"
];

export const port = 3000;

export const env = process.env.NODE_ENV || "development";

export const epoch = 1420070400000;

export const serverSalt = process.env.SERVER_SALT || "thanos";

export const objectStorage = {
  endpoint: "play.minio.io",
  port: 9000,
  useSSL: true,
  auth: {
    access: "Q3AM3UQ867SPQQA43P2F",
    secret: "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG"
  },
  region: "us-east-1"
};

export const fileBucket = "zoomer-files";

export const shortcodes = {
  paranoid: 8,
  normal: 4
};

export const secure = env == "production";
export const externalPort =
  env == "production" ? (secure ? "443" : "80") : port;
