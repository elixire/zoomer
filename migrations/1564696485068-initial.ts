import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1564696485068 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "domain" ("id" character varying NOT NULL, "domain" character varying(20) NOT NULL, "ownerId" character varying NOT NULL, "adminOnly" boolean NOT NULL DEFAULT false, "official" boolean NOT NULL DEFAULT false, "wildcard" boolean NOT NULL DEFAULT false, "permissions" integer NOT NULL DEFAULT 3, CONSTRAINT "UQ_27e3ec3ea0ae02c8c5bceab3ba9" UNIQUE ("id"), CONSTRAINT "UQ_18338223126edde6a141cf3888c" UNIQUE ("domain"), CONSTRAINT "PK_27e3ec3ea0ae02c8c5bceab3ba9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" character varying NOT NULL, "username" character varying(20) NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "admin" boolean NOT NULL DEFAULT false, "active" boolean NOT NULL DEFAULT false, "fileLimit" integer NOT NULL DEFAULT 104857600, "shortenLimit" integer NOT NULL DEFAULT 100, "domainId" character varying NOT NULL DEFAULT '0', "shortenDomainId" character varying NOT NULL DEFAULT '0', "subdomain" character varying NOT NULL DEFAULT '', "shortenSubdomain" character varying NOT NULL DEFAULT '', "consented" boolean DEFAULT null, "paranoid" boolean NOT NULL DEFAULT false, CONSTRAINT "UQ_cace4a159ff9f2512dd42373760" UNIQUE ("id"), CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "file" ("id" character varying NOT NULL, "domainId" character varying, "ownerId" character varying NOT NULL, "size" integer NOT NULL, "shortcode" character varying NOT NULL, "extension" character varying NOT NULL, "subdomain" character varying DEFAULT null, "mime" character varying NOT NULL DEFAULT 'application/octet-stream', "available" boolean NOT NULL DEFAULT true, CONSTRAINT "UQ_36b46d232307066b3a2c9ea3a1d" UNIQUE ("id"), CONSTRAINT "PK_36b46d232307066b3a2c9ea3a1d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "shorten" ("id" character varying NOT NULL, "domainId" character varying NOT NULL DEFAULT 0, "ownerId" character varying NOT NULL, "location" character varying NOT NULL, "shortcode" character varying NOT NULL, "subdomain" character varying NOT NULL, CONSTRAINT "UQ_f43226bede2b1ff5f2e2a45d0ec" UNIQUE ("id"), CONSTRAINT "PK_f43226bede2b1ff5f2e2a45d0ec" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_31372126642a18acf9110981319" FOREIGN KEY ("domainId") REFERENCES "domain"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_e7e7efd02e415c2d828e8d67bd6" FOREIGN KEY ("shortenDomainId") REFERENCES "domain"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "file" ADD CONSTRAINT "FK_c157bf6008bd8b68fa14bf2923c" FOREIGN KEY ("domainId") REFERENCES "domain"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "file" ADD CONSTRAINT "FK_34c5a7443f6f1ab14d73c5d0549" FOREIGN KEY ("ownerId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "shorten" ADD CONSTRAINT "FK_048611a8320bb3a1f11644d0db6" FOREIGN KEY ("domainId") REFERENCES "domain"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "shorten" ADD CONSTRAINT "FK_c214746932d5e5487b59c449959" FOREIGN KEY ("ownerId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "shorten" DROP CONSTRAINT "FK_c214746932d5e5487b59c449959"`);
        await queryRunner.query(`ALTER TABLE "shorten" DROP CONSTRAINT "FK_048611a8320bb3a1f11644d0db6"`);
        await queryRunner.query(`ALTER TABLE "file" DROP CONSTRAINT "FK_34c5a7443f6f1ab14d73c5d0549"`);
        await queryRunner.query(`ALTER TABLE "file" DROP CONSTRAINT "FK_c157bf6008bd8b68fa14bf2923c"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_e7e7efd02e415c2d828e8d67bd6"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_31372126642a18acf9110981319"`);
        await queryRunner.query(`DROP TABLE "shorten"`);
        await queryRunner.query(`DROP TABLE "file"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "domain"`);
    }

}
